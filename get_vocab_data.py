#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 11:49:11 2020

@author: prithvi, aman
"""

import pandas as pd 
import nltk
from pycontractions import Contractions

def generate_data(folder, file):
    data = pd.read_csv('data/%s' % file)

    titles = data['Title']
    abstracts = data['Abstract']
    
    vocab_title = {}
    vocab_abstract = {}
    
    cont = Contractions('GoogleNews-vectors-negative300.bin')
    tokenizer = nltk.RegexpTokenizer(r'[^\W_]+|[^\W_\s]+')
    
    title_tokens = 0
    for i in range(len(titles)):
        words = tokenizer.tokenize(list(cont.expand_texts([titles[i]], precise=True))[0])
        title_tokens += len(words)
        for word in words:
            if (word not in vocab_title.keys()):
                vocab_title[word] = 1
            else:
                vocab_title[word] += 1
                
    sort_vocab_titles = sorted(vocab_title.items(), key=lambda x: x[1], reverse=True)
    vocab_title = []
    for i in range (len(sort_vocab_titles)):
        vocab_title.append([sort_vocab_titles[i][0], sort_vocab_titles[i][1]])
    
    
    abstract_tokens = 0
    for i in range(len(abstracts)):
        words = tokenizer.tokenize(list(cont.expand_texts([abstracts[i]], precise=True))[0])
        abstract_tokens += len(words)
        for word in words:
            if (word not in vocab_abstract.keys()):
                vocab_abstract[word] = 1
            else:
                vocab_abstract[word] += 1
                
    sort_vocab_abstracts = sorted(vocab_abstract.items(), key=lambda x: x[1], reverse=True)
    vocab_abstract = []
    for i in range (len(sort_vocab_abstracts)):
        vocab_abstract.append([sort_vocab_abstracts[i][0], sort_vocab_abstracts[i][1]])
        
    vocab_title = pd.DataFrame(vocab_title, columns=['word', 'count'])
    vocab_title.to_csv('data/%s/title_vocabulary.csv' % folder, index=False)
    vocab_abstract = pd.DataFrame(vocab_abstract, columns=['word', 'count'])
    vocab_abstract.to_csv('data/%s/abstract_vocabulary.csv' % folder, index=False)
    token_type_ratio = pd.DataFrame([[title_tokens/len(vocab_title), abstract_tokens/len(vocab_abstract)]], columns=['title', 'abstract'])
    token_type_ratio.to_csv('data/%s/token_type_ratio.csv' % folder, index=False)
    

dataset = [['bio', 'QuantitativeBiology.csv'], 
           ['computer', 'ComputerScience.csv'], 
           ['finance', 'QuantitativeFinance.csv'], 
           ['maths', 'Mathematics.csv'], 
           ['physics', 'Physics.csv'], 
           ['stats', 'Statistics.csv']]

for data in dataset:
    generate_data(data[0], data[1])