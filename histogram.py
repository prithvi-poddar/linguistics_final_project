#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 02:54:10 2020

@author: prithvi
"""

import pandas as pd
import numpy as np
from tools import *
from matplotlib import pyplot as plt
# =============================================================================
# NOUN (nouns)
# VERB (verbs)
# ADJ (adjectives)
# ADV (adverbs)
# PRON (pronouns)
# DET (determiners and articles)
# ADP (prepositions and postpositions)
# NUM (numerals)
# CONJ (conjunctions)
# PRT (particles)
# . (punctuation marks)
# X (a catch-all for other categories such as abbreviations or foreign words)
# =============================================================================
  
tags = list(get_abstract_universal_tags_dictionary())

count = 0
for tag in tags:
    
    count += 1 
    
    if (count == 1):
        name = 'nouns'
    elif (count == 2):
        name = 'verbs'
    elif (count == 3):
        name = 'adpositions'
    elif (count == 4):
        name = 'adjectives'
    elif (count == 5):
        name = 'particles'
    elif (count == 6):
        name = 'conjunctions'
    elif (count == 7):
        name = 'punctuations'
    elif (count == 8):
        name = 'adverbs'
    elif (count == 9):
        name = 'determinants'
    elif (count == 10):
        name = 'pronouns'
    elif (count == 11):
        name = 'foreign_words'
    elif (count == 12):
        name = 'cardinals'
        
    
    subject = list(tag.keys())
    counts = list(tag.values())
      
    
    # Figure Size 
    fig, ax = plt.subplots(figsize =(20, 10)) 
      
    # Horizontal Bar Plot 
    ax.barh(subject, counts) 
      
    # Remove axes splines 
    for s in ['top', 'bottom', 'left', 'right']: 
        ax.spines[s].set_visible(False) 
      
    # Remove x, y Ticks 
    ax.xaxis.set_ticks_position('none') 
    ax.yaxis.set_ticks_position('none') 
      
    # Add padding between axes and labels 
    ax.xaxis.set_tick_params(pad = 5) 
    ax.yaxis.set_tick_params(pad = 10) 
      
    # Add x, y gridlines 
    ax.grid(b = True, color ='grey', 
            linestyle ='-.', linewidth = 0.5, 
            alpha = 0.2) 
      
    # Show top values  
    ax.invert_yaxis() 
      
    # Add annotation to bars 
    for i in ax.patches: 
        plt.text(i.get_width()+0.2, i.get_y()+0.5,  
                 str(round((i.get_width()), 2)), 
                 fontsize = 10, fontweight ='bold', 
                 color ='grey') 
      
    # Add Plot Title 
    ax.set_title('Fraction of %s in various classes of research abstracts' % name, 
                 loc ='left', ) 
      
    # Add Text watermark 
    fig.text(0.9, 0.15, 'Linguistics Final Project\nby Prithvi and Aman', fontsize = 12, 
             color ='grey', ha ='right', va ='bottom', 
             alpha = 0.5) 
      
    # Show Plot 
    plt.savefig('%s_histogram.png' % name) 