#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 22:29:05 2020

@author: prithvi
"""

import nltk
import pandas as pd
import numpy as np
from tools import *
from matplotlib import pyplot as plt
from nltk import ngrams

file_bigram = "tag_bigrams.csv"
file_trigram = "tag_trigrams.csv"
file_4gram = "tag_4grams.csv"
file_5gram = "tag_5grams.csv"
file_6gram = "tag_6grams.csv"

def ngram_from_csv(file):
    bio = pd.read_csv('data/bio/%s' % file)
    comp = pd.read_csv('data/computer/%s' % file)
    fin = pd.read_csv('data/finance/%s' % file)
    math = pd.read_csv('data/maths/%s' % file)
    phy = pd.read_csv('data/physics/%s' % file)
    stats = pd.read_csv('data/stats/%s' % file)
    
    return bio, comp, fin, math, phy, stats

bio, comp, fin, math, phy, stats = ngram_from_csv(file_trigram)