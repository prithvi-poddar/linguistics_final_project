#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 09:28:38 2020

@author: prithvi, aman
"""


# =============================================================================
# TO DO:
#     bigrams and trigrams
#     ratios
#     histograms
# =============================================================================
    


import pandas as pd 
import nltk
from nltk.tag import StanfordPOSTagger
from nltk.tokenize import word_tokenize
jar = '/home/prithvi/nltk_data/taggers/stanford-postagger-full-2020-11-17/stanford-postagger.jar'
model = '/home/prithvi/nltk_data/taggers/stanford-postagger-full-2020-11-17/models/english-bidirectional-distsim.tagger'


def stanford_tagger(folder, file):
    data = pd.read_csv('data/%s' % file)

    titles = data['Title'].tolist()
    abstracts = data['Abstract'].tolist()
    
    del data
    
    """ For sequencing, we use the Stanford POS Tagger"""
    
    st = StanfordPOSTagger(model, jar, encoding = "utf-8")
    
    title_tags = []
    abstract_tags = []
    
    for i in range(len(titles)):
        sent_t = titles[i]
        sent_a = abstracts[i]
        
        tag_t = st.tag(nltk.word_tokenize(sent_t))
        tag_a = st.tag(nltk.word_tokenize(sent_a))
        
        sentence = tag_t[0][1]
        for j in range(1, len(tag_t)):
            sentence += ' '+tag_t[j][1]
        
        title_tags.append(sentence)
        
        sentence = tag_a[0][1]
        for j in range(1, len(tag_a)):
            sentence += ' '+tag_a[j][1]
        
        abstract_tags.append(sentence)
        
        
    title_tags = pd.DataFrame(title_tags)
    abstract_tags = pd.DataFrame(abstract_tags)
    title_tags.to_csv('data/%s/title_tags.csv' % folder, index=False)
    abstract_tags.to_csv('data/%s/abstract_tags.csv' % folder, index=False)


dataset = [['finance', 'QuantitativeFinance.csv'], 
           ['maths', 'Mathematics.csv'], 
           ['physics', 'Physics.csv'], 
           ['stats', 'Statistics.csv'],
           ['computer', 'ComputerScience.csv']]

for data in dataset:
    stanford_tagger(data[0], data[1])
    print(data[1]+" done")
        
#['bio', 'QuantitativeBiology.csv'], 